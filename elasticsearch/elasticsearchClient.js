const elasticsearch = require('elasticsearch');

const elasticsearchClient = new elasticsearch.Client({
  host: 'localhost:9200'
});

elasticsearchClient.ping({
  requestTimeout: 1000
}, (error) => {
  if (error) {
    console.trace('elasticsearch cluster is down!');
  } else {
    console.log('elasticsearch is up and running');
  }
});

module.exports = elasticsearchClient;
