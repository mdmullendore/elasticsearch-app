# README

Elasticsearch search engine.

## Tech Stack

* Express
* Elasticsearch

## Starting server

```
npm run dev
```

## Elasticsearch

https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/16.x/index.html
