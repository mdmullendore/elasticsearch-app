const express = require('express');
const app = express();
const articles = require('./api/articles');

let port = process.env.PORT || 3000;

app.use('/api', articles);

app.listen(port, () => {
  console.log(`Server has started on port ${port}`);
});
