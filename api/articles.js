const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser').json();
const elasticsearchClient = require('../elasticsearch/elasticsearchClient');

router.use(async (req, res, next) => {
  try {
    const response = await elasticsearchClient.index({
      index: 'logs',
      body: {
        url: req.url,
        method: req.method
      }
    });

    if (response) {
      console.log('logs indexed');
      next();
    } else {
      throw 'unable to index';
    }
  } catch (err) {
    console.log(err);
    next();
  }
});

router.post('/articles', bodyParser, async (req, res) => {
  try {
    const articles = req.body;

    const response = await Promise.all(articles.map(async (article) => {
      return await elasticsearchClient.index({
        index: 'articles',
        body: article
      });
    }));

    if (response) {
      res.status(200).json({
        msg: `Articles have been index at: ${Date.now()}`
      });
    } else {
      throw 'Error indexing articles';
    }
  } catch(err) {
    res.status(500).json({
      message: 'Error indexing articles',
      error: err
    });
  }
});

router.put('/articles/:id', bodyParser, async (req, res) => {
  try {
    const response = await elasticsearchClient.update({
      index: 'articles',
      id: req.params.id,
      body: {
        doc: req.body
      }
    });

    if (response) {
      res.status(200).json({
        message: `article id: ${req.param.id} has been updated`
      });
    } else {
      throw `Unable to update article id: ${req.params.id}`;
    }

  } catch (err) {
    res.status(500).json({
      message: `Unable to update article id: ${req.params.id}`,
      error: err
    });
  }
});

router.get('/articles/:id', async (req, res) => {
  try {
    const response = await elasticsearchClient.get({
      index: 'articles',
      body: req.params.id
    });

    if (response) {
      const statusCode = response ? 200 : 404;
      res.status(statusCode).json({
        article: response
      });
    } else {
      throw `Unable to process request for article ${req.params.id}`;
    }
  } catch(err) {
    res.status(500).json({
      message: `Unable to process request for article ${req.params.id}`,
      error: err
    });
  }
});

router.get('/articles', async (req, res) => {
  try {
    let query = {
      index: 'articles'
    };

    if (req.query.filter) {
      query.q = req.query.filter;
    }

    const response = await elasticsearchClient.search(query);

    if (response) {
      res.status(200).json({
        articles: response.hits.hits
      });
    } else {
      throw `Unable to process query`;
    }
  } catch (err) {
    res.status(500).json({
      message: `Unable to process query`,
      error: err
    });
  }
});

router.delete('/articles/:id', async (req, res) => {
  try {
    const response = await elasticsearchClient.delete({
      index: 'articles',
      id: req.params.id
    });

    if (response) {
      res.status(200).json({
        message: `Article id: ${req.params.id} has been deleted`
      });
    } else {
      throw `Unable to delete article id: ${req.params.id}`;
    }
  } catch(err) {
    res.status(500).json({
      message: `Unable to delete article id: ${req.params.id}`,
      error: err
    });
  }
});

router.delete('/articles', async (req, res) => {
  try {
    const response = await elasticsearchClient.delete({
      index: 'articles'
    });

    if (response) {
      res.status(200).json({
        message: `Article id: ${req.params.id} has been deleted`
      });
    } else {
      throw `Unable to delete articles`;
    }
  } catch(err) {
    res.status(500).json({
      message: `Unable to delete articles`,
      error: error
    });
  }
});

module.exports = router;
